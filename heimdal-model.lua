local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
fs = require("acf.fs")

-- Set variables
local configfile = "/etc/krb5.conf"
local packagename = "heimdal"

-- ################################################################################
-- LOCAL FUNCTIONS

-- ################################################################################
-- PUBLIC FUNCTIONS

function mymodule.getstatus()
	return modelfunctions.getstatus(nil, packagename, "Heimdal Status")
end

function mymodule.get_filedetails()
	return modelfunctions.getfiledetails(configfile)
end

function mymodule.update_filedetails(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, {configfile})
end

function mymodule.get_kinit()
	local value = {}
	value.login = cfe({ label="KDC login", seq=1 })
	value.password = cfe({ type="password", label="KDC password", seq=2 })
	return cfe({ type="group", value=value, label="Kinit Parameters" })
end

function mymodule.set_kinit(self, data)
	local tmp = "/tmp/k"..os.time()
	fs.write_file(tmp, data.value.password.value)
	data.descr, data.errtxt = modelfunctions.run_executable({"kinit", "--password-file="..tmp, data.value.login.value}, true)
	os.remove(tmp)
	if not data.errtxt and data.descr == "" then data.descr = "Success" end
	return data
end

function mymodule.klist()
	local result, errtxt = modelfunctions.run_executable({"klist"})
	if not errtxt and result == "" then result = "No tickets found" end
	return cfe({ value=result, label="List of Kerberos Tickets", errtxt=errtxt })
end

function mymodule.get_kdestroy()
	return cfe({ type="group", value={}, label="Destroy Tickets" })
end

function mymodule.kdestroy(self, kd)
	kd.descr, kd.errtxt = modelfunctions.run_executable({"kdestroy"})
	if not kd.errtxt and kd.descr == "" then kd.descr = "Successfully Destroyed Tickets" end
	return kd
end

return mymodule
