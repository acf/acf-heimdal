<% local data, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<% htmlviewfunctions.displaycommandresults({"kdestroy"}, session) %>
<% htmlviewfunctions.displaycommandresults({"kinit"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(data, page_info) %>
<pre>
<%= html.html_escape(data.value) %>
</pre>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("kdestroy") then
	viewlibrary.dispatch_component("kdestroy")
end %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("kinit") then
	viewlibrary.dispatch_component("kinit")
end %>
