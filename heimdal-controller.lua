local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.get_filedetails, self.model.update_filedetails, self.clientdata, "Save", "Edit Heimdal Config", "Configuration Set")
end

function mymodule.klist(self)
	return self.model.klist()
end

function mymodule.kinit(self)
	return self.handle_form(self, self.model.get_kinit, self.model.set_kinit, self.clientdata, "Submit", "Obtain Kerberos Ticket")
end

function mymodule.kdestroy(self)
	return self.handle_form(self, self.model.get_kdestroy, self.model.kdestroy, self.clientdata, "Destroy", "Destroy Tickets")
end

return mymodule
